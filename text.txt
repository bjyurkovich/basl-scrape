


Hi, Kathy. Thank you for being here with us today. 


Specifically during the covid crisis, Uber has been fortunate enough to champion its other lines of business outside of the typical rider and driver product. Ubereats has been a great alternative for users getting on the road and going out to eat, and many restaurants have adopted a delivery-first business model to adjust to the changing times. For us, re-engagement during this uncertain time has become an opportunity to think more critically about our product messaging. Our previous focus was measured in hard metrics like business impact, and not enough on the human experience. We’re still working on finding the right balance of how to speak to our customers while adapting to the quickly evolving marketplace, building up a new user base that had previously not considered using options outside of our rider/driver product lines. 


Growth has been a core function at Uber for many years, and was a small founding team of scrappy channel marketers when I joined 4.5 years ago. We have grown in headcount, channels managed, products supported, and number of initiatives over all these years. I experienced the largest growth in the team in 2016 when paid marketing was scaling globally. This was one of the most exciting times in my career as market expansion and product adoption was hitting its stride globally; there were so many growth levers we had in our arsenal to test and learn from. This explosive growth taught me 2 valuable lessons: Scaling and evaluating growth changes over time - foundational measurement tools are essential not only at the channel level, but across all types of marketing; the unit economics are important, but setting up programs for the long term keeps us competitive. 


There is no “secret sauce” for growth, but a high velocity of experiments and a culture grounded in experimentation is one of the elements that fuel overall growth. In the early days we had a motto to “always be testing” and have maintained a very results-oriented environment over the years. When our experiments are not seeing a lot of wins, we don’t see this as a shortcoming or failure, but rather a challenge to use data to inform new ways of working. In order to gamify our program wins and losses, we now have an ongoing tally of our experiment predictions which makes for a fun competitive spirit. In fact, I often think the frustration of having few wins is more motivating than having only wins, but who knows, if I start to run tons of stat sig negative experiments, I may reconsider this outlook. ;)


Hi, Kathy.


From my experience, the main difference between these two types of audiences is the length of the onboarding journey. Because of this disparity, the differences in growth strategies vary. What’s worked for Uber is personalized, but short to-the-point ads (eg: mobile ads on paid channels like FB and display) that drive home concise messaging for B2C. On the other hand for B2B paid marketing is a lever to drive traffic to a web page where users will do further discovery. I’ve seen it more to get a B2B buyer to build product education and we can’t make a judgment call on what is working and what’s not by simply turning on/off a campaign. B2B requires many more ad impressions and site visits to increase their consideration and a lot of supplementary content like a CRM drip campaign for those who sign up. 


Hello Kathy and thank you in advance for sharing hour insights.


At a personal level, I’d say the tactics I’ve used are to be adaptable to impactful in ANY workplace rather than having goals oriented towards reaching a specific seniority at a specific company. In fact, I started out in marketing unconventionally as I majored in architecture, and had no prior experience or network in marketing until my last semester of undergrad. I was fortunate to be given an opportunity for a digital marketing internship despite having no prior experience, which over the course of 3 months, turned into a full time position. Long story short, it’s about taking ownership of unconventional opportunities, and leaning in every day to learn and hustle. Those were things I learned from architecture, not because I had a formal education in marketing, but they translate across all industries. Also, don’t start a new role with a list of things you don’t want to do - I see this mistake frequently; people join teams already knowing what they're NOT willing to try. Where's the growth in that? Uber is a very results-oriented environment which is right in my wheelhouse; I let my experiments, campaigns, and program results speak for themselves. I was lucky enough that Uber's mindset already aligned with mine, and my peers helped me deliver on these promises. That said, the mentality that’s worked best for me (separate from the tactics), is that I’ve also always worked to improve on, is to take the opportunities to solicit feedback frequently. This has helped me gain momentum to learn and grow in new roles and made me flexible to learning new things and taking on new scope and responsibilities. Asking others for their feedback is so critical to your own growth as a person and instead of struggling on your own acting with only limited information, and creates a great dialogue that no longer feels like a critique. 


When running install ads on Facebook, which link attribution platform do you recommend? Thoughts on branch, kochava, AppsFlyer?


From my experience, Branch (previously Tune) has been easy to use and already plays best with Facebook for postback attribution and cross domain tracking which has been tricky. I have limited experience with AppsFlyer, but hear great things about it. 


Assuredly this problem gets easier to solve the more well-known your company becomes, and the more resources you have at your disposal, but can you speak to some of the difficulties that arise when trying to launch Uber in a new market? What are some of the strategies you’ve found help overcome them? (As an example, does focusing your marketing efforts on a few concentrated neighborhoods first, before expanding across a city, help?)


When introducing a new service like Uber to a new geography or region, the growth marketing team was all centralized out of our HQ office. We soon realized that we’d approach all new markets similarly and with the same channel mix. So we started to test and learn which of the core channels worked best in each new market; obviously some self-serve channels were easier to prop up and run tests, while others took more heavy lifting. However, because the consumption of media is so different by market, there is no one-size-fits all playbook to scale marketing across the world. We quickly pivoted to hiring out teams in-region and put a huge importance on localizing regional marketing creatives so that each region felt supported and our marketing message included local context and messaging. For example, something as simple as a picture of a car required localization because the steering wheels are on the opposite side of the car when cars drive on the other side of the road. Also, a food themed creative that works in the US is unlikely to work in another region, pairing local dishes with country specific targeting is key. These are small considerations, but are the difference makers between producing creative message that resonates versus alienates.


Hi Kathy - Nice to e-meet you! Do you have any best practices or do's and don'ts for freemium offers/programs? Also, how best are they managed operationally?


While my experience with freemium services doesn’t come from Uber directly, I would recommend focusing on a KPI that correlates more closely with the paid subscription rather than the trial. When I think about freemium trials, acquiring many users to the free version of the product is only the first step, not the most valuable step, and can be mistakenly attributed to success. The KPI or action that you’d be best off measuring is an action that can be used as a proxy for users turning into paid customers. This can be found by the actual actions taken in the product’s app/website, like adding information to an account, uploading a doc, trying a certain feature, etc. It may be an action that is somewhat hidden, but as long as it facilitates the action that correlates strongly to an actual indicator of quality, you may not see as big of an uptick in the free subscribers, but generate sticker customers and build campaigns around optimizing towards those actions later in the user’s journey. 


Hi Kathy, thanks for doing this.
Do you feel Uber has reached peak market saturation here in the US?
How do you differentiate the service from Lyft now that so many drivers work for both companies?


The answer here swings wildly depending on which Uber product; Uber has opportunistically diversified its product lines to micromobility, grocery delivery, etc. which allows us to have a sizable TAM even after 11 years of operation. As you’d expect, there are many Uber riders already on the platform in the US, but there are always more users we can acquire; and this is being explored more by the product teams to create new features and options to suit the use cases of these much later wave of adopters more so than on the paid marketing side. That said, the narrative to build up our driver acquisition program will definitely take on a new tone and lens as covid restrictions lift; I believe there will be a pretty large addressable market for this line of business that is motivated by different factors due to everyone adapting to the world impacted by covid. On the other hand, there’s a huge opportunity to acquire more Eaters and develop new consumption patterns for food delivery during covid and we have the opportunity to encourage them to form a new habit of ordering from Ubereats in the mid to long term as we reach a “new normal”. 


What channels do you use for Re-targeting at Uber? and, any specific objectives, targeting any type of audiences.
Any specific strategies for holidays, events, etc..?
If you were marketing for a smaller company in the outdoor industry, which channels would you utilize for re-targeting.


 Paid channels used for retargeting and re-engagement only became a tactic explored at Uber as markets matured and acquisition and growth was diminishing in incrementality. Instead of a single target audience, we focused on testing the efficacy of paid marketing on many different cohorts to see where we could ignite a direct response (eg: incremental trip). Outside of our typical CRM owned and operated channels, we made a strategic decision to heavy up on paid once we felt we had a solid metric and cost per incremental action to hit. Paid social, programmatic display, and affiliates have played a big role in our retargeting and re-engagement strategy, either working in tandem to boost incrementality when run together, or running channel-specific tests. These platforms each have solid adtech stacks that can help with incrementality measurement and conversion tracking to ensure we are being effective. Where we can’t rely on 3rd party conversion lift tools, we have some pretty awesome home-grown solutions for testing and measurement. With Uber’s test and learn mentality, we’ve retargeted so many different audiences over the last few years, paired with personalized creative, in order to see where we can drive the most incremental conversion value. 


In order to conduct remarketing for a company in the outdoor industry, I don’t believe the strategies are unique depend on the industry, but rather the data that you’re able to collect and gather insights from. How are you establishing your 1st touchpoint to then build a list to retarget off of? On a website, this can be as simple as placing an ad pixel on the homepage or a specific site event and then you can retarget using some fairly cost effective self serve channels like DV360, programmatic display, social, and most other digital channels for that matter. On the other hand, if your touchpoint is in-store, the only data you’ve collected might be signups or phone #s which allow you to retarget using email or SMS CRM and possibly direct mail if you have relationships with data venders in this space. 


Hi Kathy, please can you tell us more about re-marketing and re-engagement across paid channels for Uber?


I answered some similar questions elsewhere in this forum - but do let me know if you have follow-up questions.


Hi Kathy!
I wanted to know If you followed a north star metric framework and if you did, what was your mid-term goal & NSM. In case you didn't please explain why. 


In almost all cases and all product lines, each business has a single northstar metric and associated KPIs and proxies for performance so that each is focused on achieving no more than one thing at a time. Easier said than done. They have ranged from metrics as simple as signups or installs in the very early days, to first trips or events that were better indicators of on platform activity. For a long time we lived and breathed these direct response metrics. We’ve since moved on to other metrics with more complexities baked in. From my past experiences, there were certainly instances where the performance marketing team tried to tackle direct response, awareness, and brand all in one campaign, and that made for a LOT of lessons learned the hard way.


Hi Kathy,


I answered a similar question about the best growth hack at Uber elsewhere in this forum - but do let me know if you have follow-up questions.


Hi Kathy,


The approach to other digital channels that don’t fare well with a basic click through direct response attribution model are often measured by alternative KPIs that still balance brand with quality such as a social engagement. We’ve run market level tests to turn on OOH, radio, and TV - comparing the behavior of customers in similarly sized/similarly dense cities that have offline ads on, and cities that we’ve kept as a control group with no ads running. Our teams manage campaign types based on business objectives, but our adtech and analytics teams support all measurement for these hard-to-measure channels. 


Hi Kathy, we sent in a question 6 days ago, do you see it? Thanks, Jared


Hey, there! She is answering all the questions today ;)


How did you manage to balance growth with riders and drivers especially in the early stages?


Hi Kathy - thanks for taking the time to do this. 


What is The process in uber from an idea to The excecution process? Thank you very much Kathy


We’ve toyed with many different processes at uber, whether they be setting up SMART goals or conducting projects using the RAPID framework, the consistent mentality though has always been more thematic, “let the fiercest truth seekers rise to the top”. While this is an old Uber saying, it is the best way to encapsulate our methodology. While the company has matured, what's held true is that we never assume anything works until we test them. We take a look at the market opportunity and the size of that opportunity and structure our ideation process. We build hypotheses and ground our business opportunities in existing data, and that makes stakeholder buy in much easier to streamline -whether we are talking to engineers, designers, analysts, or leadership- so when we have wins, we can double down on successful experiments. 


Hi Kathy,


At Uber, we’re lucky to have an organized creative asset management system that is owned by creative optimization channel managers. We also have huge sets of dynamic creatives that powered by a feed of creative elements. By doing this, we don’t have to create countless folders of static creatives for every variation. This was not always the case, so we’ve built an in-house repository for tracking purposes. 


We used to have weekly channel meetings and a monthly marketing review (MMR) to analyze channel specific trends and run through weekly growth metrics with a monthly retrospect to reflect on what’s working and what can be improved. We also host ideation sessions and backlog grooming to brainstorm in a forum and prioritize ideas into a queue (ranked on a tradeoff between business impact and level of effort), then execute, analyze, and repeat until we’ve landed a successful growth strategy which we can put into production or evergreen.


Hi, thanks for sharing your expertise 


Between replies to various questions in this forum, I believe I've answered this question - but do let me know if you have follow-up questions.


Hi, Kathy.
Do you use any propensity indicator to identify users who are more likely to convert and use this scoring in retargeting campaigns?


Definitely! We have a trove of 1st party engagement metrics from site or app interactions that we can use as proxies or signals of propensity to do/not take certain actions. We have a talented data science team that builds and trains propensity models to inform our target audiences and another team of analysts who have amazing data fluency to take deeper dives to find unconventional insights. While these indicators can inspire our next campaigns or experiments, there is no substitute for testing and only through a series of experiments can we find that sweet spot of efficiency and scale when optimizing for these signals.


Hi Kathy,


Totally thought this was a webinar AMA. I'd like to also add...


I may have touched on this in a previous answer, but at university, I was formally educated in architecture, not marketing, econ, or business, but what I learned there was how to be ambitious, endure the all nighters, and how to learn from a talented highly motivated set of peers; this was made especially difficult because in design, your work never felt like it was done. I can’t pinpoint a specific strategy I employed, but being given an opportunity to succeed at a place like Uber was already a huge door that had been opened; the rest was up to me. I brought this same attitude to Uber, and I found in my first few months at Uber, that I was surrounded by those who had the same mindset that I had - a team with a strong appetite to learn and drive results. One of the best company values back in the day was “always be hustling” and whether you abide by it or not, it’s certainly one of the reasons I’m still highly motivated today. In order to feel empowered and encouraged, I find that it’s best that I hold myself accountable above all else.
When you’re thinking about growing your career, there is no better time to start than now. You can always find excuses to put it off, but one of the biggest things I fear in the back of my mind is complacency. Not sure where this quote came from, but it really resonates with me “you are the CEO of your own life.” Everyone wants to be the CEO of their own company, and the wins that come with it, but why not start by having that accountability in your own life, regardless of what role you hold and what point you’re at in your career? I have been able to use this time at home during quarantine to upskill for whatever comes next - whether that is a new project, broader scope, or just deciphering what I want out of a future role. While you don’t lean in one day and try to hit your long term goal by next week, I have regular check-ins with my manager, ex-manager, ex-ex-manager (you see where I’m going with this), to make sure I’m willing to maintain the open dialogue necessary to stay on track to hit these larger milestones. 



What's the best growth hack that helped Uber grow ?


The short answer is, there are no shortcuts to growth hacking. The closest thing I’ve seen to a playbook resource is Traction: How Any Startup Can Achieve Explosive Customer Growth  which is a structured way to ideate on and try out some tried and true digital channels. The traffic sources that were successful at Uber 4.5 years ago are no longer the same ones that are successful now for a variety of reasons - diminishing incrementality at the channel or ad level, limited unique reach, a more saturated market due to more advertisers on the platform, market saturation of the Uber rider product. In fact, back when I started at Uber, there weren’t even the same mediums for paid channels as there are now. Since starting at Uber, Google has rolled out UAC, Snapchat has created a robust ad platform (we were one of the first to try their beta for ads), Tik Tok became popular (and Vine disappeared). We balance the audience reach opportunity based on each platform’s expected reach/inventory management tools with what resources will be required to get a campaign off the ground, and then we prioritize and resource for the channels accordingly. 


What should be the go-to-market strategy for a new b2c freemium product from a startup? Assume in the wealth management space.


I answered a similar question about my take on how best to drive freemium product adoption and upsell elsewhere in this forum - but do let me know if you have follow-up questions.


Hi, Kathy! Thanks for the AMA.


After a successful experiment is complete, our work is not done. Once all tests have been validated and proven incremental, I work closely with engineering to roll out winning experiment variants on the website, and I work closely with creative teams to create assets for a broader roll out to the winning audience campaign. We push these experiences into production and evergreen our campaigns; at that point, the ownership is passed on to the site owner for web and the channel manager for campaigns. 


What do you focus on in your b2b marketing?
Which paid advertising platforms perform best for b2b?
Thanks Kathy :)


I answered a similar question about b2b user journeys elsewhere in this forum - but do let me know if you have follow-up questions.


Hey Kathy,


Every marketplace requires a different approach, but Uber’s growth strategies definitely pivoted towards a dual focus on re-engagement once we did deeper dives on our customer data and found that there were many within our user base (multiple product lines), had tried our product, taken a first trip (or more), but were no longer converting on the platform. This gave us a 2-pronged approach as we were given major opportunity to re-engage our customers and to preemptively keep our customers engaged as well to prevent churn in the first place. With the channel management resource on the team still remaining focused on acquisition, I was able to create new campaigns to drive re-engagement by integrating our DMP with our different publishers to pipe 1P audience data through to our partners and launch tests on a per-audience basis across multiple platforms. Once these tests were proven to be incremental, they’d roll into our evergreen marketing strategy and continue to be maintained and optimized by the respective channel expert. This way, there was ample resource dedicated to acquisition without being impeded by our on/off re-engagement testing efforts until the campaign was proven out. This also helped to usher in the incrementality testing methodology that was more widely adopted, mainly fueled by retention marketing becoming a more major initiative. That said, when I founded this program, almost everything we did was just a matter of trial and error and being willing and able to set aside test budgets and resources to ideate, test, evaluate, iterate.


Hi Kathy, The role you played a part in @ Uber is extremely impressive. I'm interested in hearing what growth skills you are dying to learn/conquer next!


I've learned and grown more in my time at Uber than any other time in my career combined. In light of the explorations, and despite the mistakes, the takeaways I have from Uber so far have helped me reach personal and professional milestones that I wouldn't have otherwise expected to hit. That said, I'm interested in focusing my efforts in a product centric role and learning how to be a better product manager so I can drive the vision, roadmap, and organizational strategy as this will in tandem with marketing, make me a better growth hacker. 


Hi,


Hi Kathy
We are due to launch a new email marketing software that has similar functionality to MailChimp
that caters to small business owners globally
The price is a fraction of what other companies charge with the aim of helping business owners reduce costs
Any advise with marketing launch would greatly appreciated 


would like to learn how to address and build an impactful remarketing strategy 