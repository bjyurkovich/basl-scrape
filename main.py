import requests
from bs4 import BeautifulSoup
base_page_url = "https://growthhackers.com/amas/ama-with-kathy-tong-growth-marketing-manager-at-uber.html"
res = requests.get(base_page_url)
soup = BeautifulSoup(res.text, 'html.parser')
content = soup.findAll("div", {"class": "comments__content"})
output = ""
for c in content:
    ps = c.find('p').text
    output = output + "\n\n\n" + ps
with open("./text.txt", "w") as f:
    f.write(output)