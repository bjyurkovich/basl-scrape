# Installation

```
git clone https://gitlab.com/bjyurkovich/basl-scrape
```

```
cd basl-scrape
virtualenv venv
source venv/bin/activate
```

Install dependencies;

```
pip install -r requirements.txt
```

Run it:
```
python main.py
```